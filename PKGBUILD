# Maintainer: Bruno Pagani <archange@archlinux.org>
# Contributor: Andrzej Giniewicz <gginiu@gmail.com>
# Contributor: Oliver Sherouse <oliver DOT sherouse AT gmail DOT com>

pkgname=python-seaborn
pkgver=0.13.1
pkgrel=1
pkgdesc="Statistical data visualization"
arch=(any)
url="https://seaborn.pydata.org/"
license=(BSD)
depends=(python-pandas python-matplotlib)
makedepends=(python-build python-installer python-flit-core)
optdepends=('python-scipy: clustering matrices and some advanced options'
            'python-statsmodels: advanced regression plots')
checkdepends=(python-pytest python-scipy python-statsmodels)
source=(https://github.com/mwaskom/seaborn/archive/v$pkgver/$pkgname-$pkgver.tar.gz)
sha256sums=('db6c126ec30fe6862ad03e81a4ff42733bd053820fcac4c3dce8c9d7033f83f7')

build() {
  cd seaborn-$pkgver
  sed -i '1591s/error/default/' tests/test_distributions.py
  sed -i '1596s/error/default/' tests/test_distributions.py
  sed -i '1757s/error/default/' tests/test_relational.py
  python -m build --wheel --no-isolation
}

check() {
  cd seaborn-$pkgver
  pytest -vv --color=yes
}

package() {
  cd seaborn-$pkgver
  python -m installer --destdir="$pkgdir" dist/*.whl
  install -Dm644 LICENSE.md -t "$pkgdir"/usr/share/licenses/$pkgname/
}

